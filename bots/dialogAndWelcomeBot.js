// Copyright (c) Microsoft Corporation. All rights reserved.
// Licensed under the MIT License.

const { CardFactory } = require('botbuilder-core');
const { DialogBot } = require('./dialogBot');


class DialogAndWelcomeBot extends DialogBot {
    constructor(conversationState, userState, dialog, logger) {
        super(conversationState, userState, dialog, logger);
		//console.log("============================== logger======================")
//console.log(logger);
        this.onMembersAdded(async context => {
			//console.log("============================== Context ======================")
//console.log(logger);
//console.log(context);

            const membersAdded = context.activity.membersAdded;
            for (let cnt = 0; cnt < membersAdded.length; cnt++) {
                if (membersAdded[cnt].id !== context.activity.recipient.id) {
                    //await context.sendActivity("Hi! I'm Irwin. It's nice to meet you.")
                        /*await new Promise(resolve => {
                            setTimeout(resolve, 1000);
                        });
                        await context.sendActivity("How can I help you today?")*/
                        await context.sendActivity({
                            attachments:[CardFactory.thumbnailCard(
                                'Support Assistant',
                
                        [{ url: 'https://image.shutterstock.com/image-vector/bot-icon-chatbot-cute-smiling-260nw-715418281.jpg' }],
                
                        [{
                
                            type: 'imBack',
                
                            title: 'Get started',
                
                            value: 'Get started'
                
                        }],
                
                        {
                
                            subtitle: 'Need help with Pharmacy related queries? Let\s Chat!',
                
                            text: 'I am designed to assist you with queries releated with Pharmcy, health and beauty \n\n---\n\n To get started, click on the below button and lets see how I can I help you.'
                
                        }
                
                    )
                            ]
                })
                }
            }
        });
    }
}

module.exports.DialogAndWelcomeBot = DialogAndWelcomeBot;
