// ========================================== IMPORT NPM MODULES ===============================//
const { LuisRecognizer } = require('botbuilder-ai');
const { CardFactory } = require('botbuilder-core');
const { ComponentDialog, DialogSet, DialogTurnStatus, TextPrompt, WaterfallDialog } = require('botbuilder-dialogs');

var MiscResponse = require('../Responses/misc_response.json')
var request = require('request');



function timeout() {
    return new Promise(resolve => {
        setTimeout(resolve, 1500);
    });
}
function qnamakerresult(result){
    return new Promise (( resolve, reject) =>{
    var options = {
        'method': 'POST',
        'url': process.env.QnAHost+'/qnamaker/knowledgebases/'+process.env.QnAKnowlegebaseid+'/generateAnswer',
        'headers': {
          'Content-type': 'application/json',
          'Authorization': 'EndpointKey '+process.env.QNAEndpoint,
        },
        body: JSON.stringify({"question":result})
      
      };
      request(options, function (error, response) {
        if (error) throw new Error(error);
        console.log("Printing body \n\n\n\n",response.body);
        resolve(response.body)
      });
    })

    }
class LuisHelper {
    /**
     * Returns an object with preformatted LUIS results for the bot's dialogs to consume.
     * @param {*} logger
     * @param {TurnContext} context
     * 
     */
    static async executeLuisQuery(logger, context) {

        try {
           
            const luisApplication = {
                applicationId: process.env.LuisAppId,
                endpointKey: process.env.LuisAPIKey,
                 endpoint: `https://${process.env.LuisAPIHostName}`,
               // endpoint: 'https://westus.api.cognitive.microsoft.com/luis/prediction/v3.0/apps/e26d625b-9838-4310-be6c-a50073594d37/slots/staging/predict?subscription-key=86bcdf3dd3d041d08eeefbb9022be78b&verbose=true&show-all-intents=false&log=true&query='
            }


            // Create configuration for LuisRecognizer's runtime behavior.
            const luisPredictionOptions = {
                includeAllIntents: true,
                log: true,
                apiVersion: 'v3',
                slot: 'production'
            }

            const luisRecognizer = new LuisRecognizer(luisApplication, luisPredictionOptions, true);
            const recognizerResult = await luisRecognizer.recognize(context);
            console.log("Inside luis recognizer", recognizerResult.luisResult.prediction.topIntent)
            // const intent = LuisRecognizer.topIntent(luisRecognizer);
            const intent = recognizerResult.luisResult.prediction.topIntent

            console.log("Intent", intent)
            

            // ========================================= GENERAL DIALOGS   =============================================================//
            if (intent == 'Greetings') {
                logger.log('************************************* GREETING_INTENT *********************************');
                context.sendActivity({ type: 'typing' });
                await timeout()
                await context.sendActivity(MiscResponse.Greeting)
            
              
            }
            else if (intent == 'intent_Capability') {
                logger.log('*************************************  CAPABILITY_INTENT *********************************');
                context.sendActivity({ type: 'typing' });
                await timeout()
                await context.sendActivity(MiscResponse.Category)
                
            }

            else if (intent == 'Miracle_Info') {
                logger.log('*************************************  MIRACLE_INFO_INTENT  *********************************');
                context.sendActivity({ type: 'typing' });
                await timeout()
                await context.sendActivity("Miracle Software Systems is a software integration service provider based out of Novi, Michigan.");
               
            }
            else if (intent == 'Miracle_Cap') {
                logger.log('************************************* MIRACLE_CAP_INTENT *********************************');
                context.sendActivity({ type: 'typing' });
                await timeout()
                await context.sendActivity("Miracle specializes in multiple fields including Cognitive services, IoT and even Augmented and Virtual reality!");
               
            }
            else if (intent == 'Miracle_bot') {
                logger.log('************************************* MIRACLE_BOT_INTENT *********************************');
                context.sendActivity({ type: 'typing' });
                await timeout()
                await context.sendActivity("Building chatbots with Miracle is as easy as 1-2-3! Just talk to one of our employees at the booth and they'll help you understand RIPS - The Rapid Innovation and Prototyping Service - the integral part of Miracle's bot building practice.");
                
            }
            
            // else if (intent == 'None') {
            //     logger.log('*************************************  NONE_INTENT *********************************');
            //     context.sendActivity({ type: 'typing' });
            //     await timeout()
            //     await context.sendActivity("I am not sure how to help with that yet. Have you checked out mInsights? Miracle's new insights platform that can help you train me better.");
                
            // }
            else if (intent == 'Thankyou') { 
                logger.log('************************************* THANKYOU_INTENT *********************************');
                context.sendActivity({ type: 'typing' });
                await timeout()
                await context.sendActivity("You're welcome. have a great day!");
              
            }
            else if(intent =='Anythingelse')
            {
                logger.log("************************************* ANYTHING_ELSE_INTENT *********************************");
                context.sendActivity({ type: 'typing' });
                await timeout()
                await context.sendActivity("I am not sure how to help with that yet. Have you checked out mInsights? Miracle's new insights platform that can help you train me better");
               
            }
            else{
                        const apiresult = await qnamakerresult(context.activity.text)
                        const qna = JSON.parse(apiresult)

             const qnaResults = qna.answers[0]
             console.log("Print context \n\n\n",  qnaResults)
             await context.sendActivity(qnaResults.answer);
            }
        } catch (err) {
            logger.warn(`LUIS Exception: ${err} Check your LUIS configuration`);

        }

    }


    /*static parseCompositeEntity(result, compositeName, entityName) {
        const compositeEntity = result.entities[compositeName];
        if (!compositeEntity || !compositeEntity[0]) return undefined;

        const entity = compositeEntity[0][entityName];
        if (!entity || !entity[0]) return undefined;

        const entityValue = entity[0][0];
        return entityValue;
    }

    static parseDatetimeEntity(result) {
        const datetimeEntity = result.entities['datetime'];
        if (!datetimeEntity || !datetimeEntity[0]) return undefined;

        const timex = datetimeEntity[0]['timex'];
        if (!timex || !timex[0]) return undefined;

        const datetime = timex[0].split('T')[0];
        return datetime;
    }*/
}

module.exports.LuisHelper = LuisHelper;
