// Copyright (c) Microsoft Corporation. All rights reserved.
// Licensed under the MIT License.

const { TimexProperty } = require('@microsoft/recognizers-text-data-types-timex-expression');
const { ComponentDialog, DialogSet, DialogTurnStatus, TextPrompt, WaterfallDialog } = require('botbuilder-dialogs');
const { Activity, ActivityTypes, Attachment, AttachmentLayoutTypes, CardAction, InputHints, SuggestedActions } = require('botframework-schema');

const { CardFactory } = require('botbuilder-core');
const { LuisHelper } = require('./luisHelper');

var Card = require('../Cards.js')

const MAIN_WATERFALL_DIALOG = 'mainWaterfallDialog';

const LUIS_HELPER = 'LuisHelper'

class MainDialog extends ComponentDialog {
    constructor(logger) {
        super('MainDialog');

        if (!logger) {
            logger = console;
            logger.log('[MainDialog]: logger not passed in, defaulting to console');
        }

        this.logger = logger;

        // Define the main dialog and its related components.
        // This is a sample "book a flight" dialog.

       
        this.addDialog(new WaterfallDialog(MAIN_WATERFALL_DIALOG, [

            this.actStep.bind(this)


        ]));

        this.initialDialogId = MAIN_WATERFALL_DIALOG;
    }


    /**
     * The run method handles the incoming activity (in the form of a DialogContext) and passes it through the dialog system.
     * If no dialog is active, it will start the default dialog.
     * @param {*} dialogContext
     */
    async run(context, accessor) {
        
        const dialogSet = new DialogSet(accessor);
        dialogSet.add(this);

        const dialogContext = await dialogSet.createContext(context);
        const results = await dialogContext.continueDialog();
        


        if (results.status === DialogTurnStatus.empty) {
            await dialogContext.beginDialog(this.id);
        }
    }
    async actStep(stepContext) {
        if(stepContext.context._activity.text == 'Get started')
        { 
            // var getStarted = stepContext.context._activity.text;
            // console.log("Welcome Message",getStarted)
            // if(getStarted == "Get started")
           // await stepContext.context.sendActivity({attachments: [CardFactory.adaptiveCard(Card["categories"])]})
           let Virtual_que = CardFactory.heroCard('', ['https://image.shutterstock.com/image-illustration/conceptual-business-illustration-words-virtual-260nw-1035168445.jpg'], ['Issue with Virtual Queue'])
           let Home_Delivery= CardFactory.heroCard('', ['https://www.netclipart.com/pp/m/169-1698406_delivery-clipart-product-delivery-door-delivery-logo-png.png'], ['Home Delivery'])
           let Track_order= CardFactory.heroCard('', ['https://www.nicepng.com/png/full/381-3818670_order-tracking-comments-order-tracking-icon-png.png'], ['Track Order'])

               await stepContext.context.sendActivity({ attachments: [Virtual_que, Home_Delivery, Track_order],attachmentLayout: AttachmentLayoutTypes.Carousel  });

            return await stepContext.endDialog();
            
        }

        else if (process.env.LuisAppId && process.env.LuisAPIKey && process.env.LuisAPIHostName) {
            
            
            // Call LUIS and gather any potential .
            var Details = {}

            Details = await LuisHelper.executeLuisQuery(this.logger, stepContext.context);
           // console.log(Details)
            return await stepContext.continueDialog();
            // if (Details.intent) {
            //     if (Details.intent == "intent_State") {
            //         this.logger.log('LUIS extracted these booking details:', Details);
            //         return await stepContext.beginDialog('stateDialog', Details)

            //     }
            //     else if (Details.intent == "intent_SetupSubstitute") {
            //         this.logger.log('LUIS extracted these booking details:', Details);
            //         return await stepContext.beginDialog('setupsubstitute', Details)
            //     }
            //     else if (Details.intent == 'intent_ForwardItem') {
            //         this.logger.log('LUIS extracted these booking details:', Details);
            //         return await stepContext.beginDialog('forwarditem', Details)
            //     }
            //     else if (Details.intent == 'intent_CurrencyEUR') {
            //         this.logger.log('LUIS extracted these booking details:', Details);
            //         return await stepContext.beginDialog('currencyeur', Details)
            //     }
            //     return await stepContext.beginDialog('confirmation', Details)


            // }
            // else {
            //     console.log("**************************************** Else **********************")
            //     return await stepContext.continueDialog();

            // }

        }
    }

}





module.exports.MainDialog = MainDialog;
